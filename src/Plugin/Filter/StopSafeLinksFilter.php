<?php

namespace Drupal\stop_safelinks\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * @Filter(
 *   id = "filter_stopsafelinks",
 *   title = @Translation("Stop Safe Links"),
 *   description = @Translation("Removes the safe link URL structure in markup."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */

class StopSafeLinksFilter extends FilterBase {
  public function process($text, $langcode) {
		$pattern = '/<a[^>]+href="https:\/\/[^\.]+\.safelinks\.protection\.outlook\.com\/\?url=([^&]+)[^>]*>/';
    $new_text = preg_replace_callback($pattern,
			function($matches) {
				$url = urldecode($matches[1]);
				\Drupal::logger('stop_safelinks')->notice('Safelink-encoded URL found: ' . $url);
				return "<a href=\"" . $url . "\">";
			},
			$text);
    return new FilterProcessResult($new_text);
  }
}