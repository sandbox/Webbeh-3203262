<?php

namespace Drupal\stop_safelinks\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginContextualInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Stop Safe Link" plugin.
 *
 * @CKEditorPlugin(
 *   id = "stopsafelink",
 *   label = @Translation("Stop Safe Links Filter"),
 *   module = "stop_safelinks"
 * )
 */
class StopSafeLinksPasteFilter extends CKEditorPluginBase implements CKEditorPluginContextualInterface {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'stop_safelinks') . '/js/stopsafelink/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * @inheritDoc
   */
  public function getButtons() {
    return [];
  }

  /**
   * @inheritDoc
   */
  public function isEnabled(Editor $editor) {
    return TRUE;
  }
}
