/**
 * @file
 * Stop Safe Links CKEditor plugin.
 *
 * Plugin for stopping those safe links!
 */

(function (Drupal) {

  'use strict';

  CKEDITOR.plugins.add('stopsafelink', {

    // Register the icons.
    icons: 'paste-filter',
    init : function( editor )
    {

      // Paste from clipboard:
      editor.on( 'paste', function(e) {
        var data = e.data,
          html = (data.html || ( data.type && data.type=='html' && data.dataValue));
        if (!html)
          return;

        // solution for Microsoft Safte Links
        html = html.replace( /<a[^>]+href="https:\/\/[^\.]+\.safelinks\.protection\.outlook\.com\/\?url=([^&]+)[^>]*>/gimsu, function(match, $1) { return '<a href="' + decodeURIComponent($1) + '">'; });

        if (e.data.html)
          e.data.html = html;
        else
          e.data.dataValue = html;
      });

    } //Init

  });

} (Drupal));
